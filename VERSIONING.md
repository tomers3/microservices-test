major.minor.path

major -> Breaking functionality changes
minor -> Backward compatiable functionality changes
patch -> Bug fixes & trivial updates


* tagging a commit
main-----
        |
        |
        |
  service branch -----
                     |
                     |
                     |
                bug fixes
                     |
                     |
                     |
                new feature
                     |
                     |
                     |
                api breaking
        
