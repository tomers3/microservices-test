pipeline {
    agent any

    stages {
        stage('CLI check') {
            when {
                changeset 'infra/cli/**'
            }
        
            stages {
                stage('Build') {
                    steps {
                        echo 'Building..'
                        sh 'sudo make -C infra/cli install'
                        sh '/usr/local/bin/dev health'
                    }
                }
                stage('Test') {
                    steps {
                        echo 'Testing..'
                        sh 'python3 -m pytest infra/cli/test'
                    }
                }
            }
        }

        stage('Services check') {
            when {
                changeset 'services/**'
            }

            stages {
                stage('Get changed services') {
                    steps {
                        echo 'Getting changed services'
                        script {
                            commiter = sh(script: "git diff --name-only $GIT_COMMIT..$GIT_PREVIOUS_COMMIT", returnStdout: true).trim()
                            files_changed = commiter.split('\n')
                            def changed_services_names = [];
                            def delimiter = '/'
                            if (System.properties['os.name'].toLowerCase().contains('windows')) {
                                println("We dont support windows for now!")
                                delimiter = '\\'
                            }

                            for (String file : files_changed) {
                                println(file)
                                file_parts = file.split(delimiter)
                                if (file_parts.length > 2 && file_parts[0] == "services") {
                                    if (!changed_services_names.contains(file_parts[1])) {
                                        changed_services_names << file_parts[1]
                                    }
                                }
                            }

                            changed_services = changed_services_names.join(" ")
                        }
                        echo "Changed Services: ${changed_services}"
                    }
                }
                stage('Test') {
                    steps {
                        echo "Testing..."
                        sh "for service in $changed_services; do /usr/local/bin/dev test \$service; done"
                    }
                }

                stage('Sync APIs') {
                    steps {
                        echo 'Syncing..'
                        sh '/usr/local/bin/dev sync'
                    }
                }
        }
    }
}
