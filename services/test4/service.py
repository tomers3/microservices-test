"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: 2022-02-28
"""
import logging
import src.test4 as test4

SERVICE_VERSION = "1.0.0"

if __name__ == "__main__":
    logger = logging.getLogger(__package__)
    logger.setLevel(logging.INFO)

    logger.info(f"Version {SERVICE_VERSION}")
    service = test4
    service.start_service()