"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: 2022-03-16
"""

import unittest
import grpc

import grpc_testing
from api.test.v1 import test_pb2
from src.test import TestServicer


class TestTest(unittest.TestCase):
    """
    Base class for testing test service
    """
    def setUp(self, _) -> None:
        super().__init__()
        servicers = {
            test_pb2.DESCRIPTOR.services_by_name['TestService']: TestServicer()
        }
        self.test_server = grpc_testing.server_from_dictionary(
            servicers, grpc_testing.strict_real_time())

    def test_send_greet(self):
        request = test_pb2.SendGreetRequest(
            msg='test',
        )
        method = self.test_server.invoke_unary_unary(
            method_descriptor=(test_pb2.DESCRIPTOR
                               .services_by_name['TestService']
                               .methods_by_name['SendGreet']),
            invocation_metadata={},
            request=request, timeout=1)

        response, metadata, code, details = method.termination()
        self.assertTrue(bool(response.status))
        self.assertEqual(code, grpc.StatusCode.OK)
