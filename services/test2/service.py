"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: 2022-03-16
"""
import logging
from src import test2

SERVICE_VERSION = "0.1.0"

if __name__ == "__main__":
    import sys
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    logging.info("Version %s", SERVICE_VERSION)
    service = test2.Test2Servicer()
    service.start_service()