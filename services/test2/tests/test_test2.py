"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: 2022-03-16
"""

import unittest
import grpc

import grpc_testing
from api.test2.v1 import test2_pb2
from src.test2 import Test2Servicer

class TestTest2(unittest.TestCase):
    """
    Base class for testing test2 service
    """
    def setUp(self, _) -> None:
        super().__init__()
        servicers = {
            test2_pb2.DESCRIPTOR.services_by_name['Test2Service']: Test2Servicer()
        }
        self.test_server = grpc_testing.server_from_dictionary(
            servicers, grpc_testing.strict_real_time())

    def test_send_greet(self):
        request = test2_pb2.SendGreetRequest(
            msg='test',
        )
        method = self.test_server.invoke_unary_unary(
            method_descriptor=(test2_pb2.DESCRIPTOR
                .services_by_name['Test2Service']
                .methods_by_name['SendGreet']),
            invocation_metadata={},
            request=request, timeout=1)

        response, metadata, code, details = method.termination()
        self.assertTrue(bool(response.status))
        self.assertEqual(code, grpc.StatusCode.OK)