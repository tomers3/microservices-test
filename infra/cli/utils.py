from __future__ import annotations
import subprocess
import git
import pathlib
import re
import logging
import os

# TODO: Add unit tests
def camel_to_snake(name):
    # name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('(?!^)([A-Z]+)', r'_\1',name).lower().replace("__", "_")

# TODO: Add unit tests
def snake_to_pascal(name):
    return name.replace("_", " ").title().replace(" ", "")

def root_path():
    script_folder = pathlib.Path(os.path.dirname(os.path.realpath(__file__)))
    return script_folder.parent.parent 

def repo():
    try:
        return git.Repo(root_path())
    except git.exc.InvalidGitRepositoryError:
        logger = logging.getLogger(__package__)
        logger.critical("Couldn't locate insoundz services git path!")
        exit(100)

def api_broke(protofiles):
    for key, item in protofiles.items():
        # We know that the path we are getting here will 
        # always be relative to <insoundz-services>/services
        ppath = pathlib.Path(next(iter(item))).relative_to(root_path())
        
        # TODO: Think if we always want to check agianst the main branch
        # or rather take the current branch
        cmd = [
            "buf", 
            "breaking", 
            "--against", ".git#branch=main",
            str(ppath)
        ]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
        stdout, _ = proc.communicate()
        breakpoint()
