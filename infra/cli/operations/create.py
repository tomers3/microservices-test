from termcolor import colored
import shutil
import logging
from jinja2 import Template
import utils
import time
from pathlib import Path
from grpc_tools import protoc

service_file_structure = {
        "[service_name_snake_case]": {
            "service.py": "templates/services/entrypoint.py",
            "src": {
                "[service_name_snake_case].py":
                    "templates/services/service_impl.py",

                "__init__.py": "templates/services/__init__.py"
            },
            "requirements.txt": "templates/services/requirements.txt",
            "api": {
                "[service_name_snake_case]": {
                    "v1": {
                        "[service_name_snake_case].proto":
                            [
                                "templates/services/default.proto",
                                "proto_file_path"
                            ]
                        },
                    },
                "pblangs": "templates/services/pblangs"
            },
            "Dockerfile": "templates/services/dockerfile",
            "tests": {
                "test_[service_name_snake_case].py":
                    "templates/services/test_grpc_service.py",

                "__init__.py": "templates/services/__init__.py"
            },
            "metadata.json": "templates/services/metadata.json"
        }
    }


# Utility functions
def create_file_from_template(template_path, dest_path, **kwargs):
    with open(utils.root_path() / "infra" / "cli" / template_path) as f:
        template_contents = f.read()
        with open(dest_path, "w") as f:
            t = Template(template_contents)
            f.write(t.render(
                kwargs,
                create_date=time.strftime("%Y-%m-%d")
            ))


def parse_name(name, service_name_snake_case, service_name_pascal_case):
    # strip suffix
    split_name = name.split(".")
    prefix = split_name[0]
    ext = "."+split_name[1] if len(split_name) > 1 else ""

    open_square_bracket = prefix.find("[")
    closing_square_bracket = prefix.find("]") + 1
    if open_square_bracket > -1 and closing_square_bracket > 0:
        prefix_name = prefix[open_square_bracket:closing_square_bracket]
        if prefix_name == "[service_name_snake_case]":
            replace_with = service_name_snake_case
        elif prefix_name == "[service_name_pascal_case]":
            replace_with = service_name_pascal_case
        else:
            raise ValueError(f"Naming convension isn't recognized {name}")

        prefix = prefix.replace(prefix_name, replace_with)
        return prefix + ext

    return name


def create_folder_struct(
        parent_path,
        folder_struct,
        service_name_snake_case,
        service_name_pascal_case,
        logger, **kwargs):

    for k, v in folder_struct.items():
        # Parse name
        parsed_name = parse_name(
                k, service_name_snake_case, service_name_pascal_case)

        if type(v) is dict:
            # Create folder
            logger.info(f"Creating folder {parsed_name}")
            folder_path = parent_path / parsed_name
            try:
                folder_path.mkdir(parents=True, exist_ok=False)
            except Exception as e:
                logger.error(f"Couldn't create folder {folder_path}")
                raise e

            create_folder_struct(
                    folder_path, v, service_name_snake_case,
                    service_name_pascal_case, logger, **kwargs)
        else:
            # Create templates
            if type(v) is list:
                kwargs_key_name = v[1]
                template = v[0] if kwargs[kwargs_key_name] is None else v[1]
            else:
                template = v

            logger.info(f"""Creating file {parsed_name}
                            from template {template}""")

            dest_path = parent_path / parsed_name
            create_file_from_template(
                    template,
                    dest_path,
                    service_name_snake_case=service_name_snake_case,
                    service_name_pascal_case=service_name_pascal_case,
                    language="python"  # TODO: Make this dynamic
                )


def rollback(path):
    logger = logging.getLogger(__package__)
    logger.info(f"Deleting path {path} (Rollback)")
    shutil.rmtree(path)


def create_service(interface):
    logger = logging.getLogger(__package__)
    logger.info(f"Attempting to create service {interface.name}")

    service_path = Path(utils.root_path() / "services")
    logger.info(f"Root directory: {service_path}")

    """
    We can also implement a rollback mechanisem on failure.
    just keep track on which folders were created and delete the top one.
    """
    try:
        create_folder_struct(
                service_path,
                service_file_structure,
                interface.snake_case,
                interface.pascal_case,
                logger,
                proto_file_path=None
            )
        return 0
    except FileExistsError as e:
        logger.error("The file/folder already exists!")
        return 5
    except Exception as e:
        logger.error(f"Unknown reason: {e}")
        # Maybe its dangerous?
        rollback(service_path / interface.snake_case)
        return 1
