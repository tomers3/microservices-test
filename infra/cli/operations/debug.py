import docker
import json
import click
import base64
import boto3
import logging
import utils
import docker_packages_injectors
import subprocess

# Theres a bug which we cant pass fileobject with context build.
# these two lines will solve this issue
import docker.api.build
docker.api.build.process_dockerfile = \
        lambda dockerfile, _: ('Dockerfile', dockerfile)


def log_docker_output(generator,
                      task_name: str = 'docker command execution') -> None:
    """
    Log output to console from a generator returned from docker client
    :param Any generator: The generator to log the output of
    :param str task_name: A name to give the task,
                          i.e. 'Build database image', used for logging
    """
    while True:
        try:
            output = generator.__next__()
            if 'stream' in output:
                output_str = output['stream'].strip('\r\n').strip('\n')
                click.echo(output_str)
            elif 'errorDetail' in output:
                raise docker.errors.BuildError(output, "error")

        except StopIteration:
            click.echo(f'{task_name} complete.')
            break
        except ValueError:
            click.echo(f'Error parsing output from {task_name}: {output}')


def debug(interface):
    """
    Debug the specified service
    """

    logger = logging.getLogger(__package__)

    lang = interface.metadata.language
    if not hasattr(docker_packages_injectors, lang):
        logger.error(f"{interface.name} implemnts unsupported language {lang}")
        return 1

    injector = getattr(docker_packages_injectors, lang).injector

    docker_low_level_client = docker.APIClient(
            base_url='unix://var/run/docker.sock')

    ecr_client = boto3.client('ecr', region_name='us-east-1')

    # TODO: Save this object in cache
    token = ecr_client.get_authorization_token()
    username, password = base64.b64decode(
            token['authorizationData'][0]['authorizationToken']
            ).decode().split(':')

    registry = token['authorizationData'][0]['proxyEndpoint'].replace(
            "https://", "")

    docker_low_level_client.login(username, password, registry=registry)

    # Build the service docker
    logger.info(f"Building {interface.name} docker image")

    # Inject language specific steps to install insoundz packages
    try:
        intermidiate_dockerfile_content = injector(
                interface.service_path / "Dockerfile")

    except docker_packages_injectors.KeywordNotFound as e:
        logger.error("""Couldn't find keyword {keyword} in
                        Dockerfile for service {service}!""")
        return 1

    try:
        log_generator = docker_low_level_client.build(
                path=str(utils.root_path()),  # Build Context
                dockerfile=intermidiate_dockerfile_content,
                tag=interface.name,
                quiet=False,
                rm=True,
                pull=True,
                decode=True
            )

        log_docker_output(log_generator)

    except docker.errors.BuildError as e:
        logging.error(f"""{interface.name} Docker build failed.
                          {e.msg['errorDetail']['message']}""")
        return 1

    except docker.errors.APIError as e:
        logging.error(f"{interface.name} Docker server has failed due to: {e}")
        return 1

    logger.info(f"Finished building {interface.name}")

    # TODO: Connect the docker to the kubernetes cluster (Telepresence)

    # run docker
    # TODO: Add a flag for production
    logger.info("Attaching container... Happy debugging :)")
    subprocess.run(['docker', 'run', '-it', interface.name])

    return 0
