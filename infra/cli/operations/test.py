import logging
import subprocess
import linters


def test_service(interface):
    """
    Run the unit tests for the given {service}
    """

    logger = logging.getLogger(__package__)

    lang = interface.metadata.language
    if not hasattr(linters, lang):
        logger.error(f"{interface.name} implemnts unsupported language {lang}")
        return 1

    # service linter
    logger.info("Running Linter")
    linter = getattr(linters, lang).linter
    lint_res = linter(interface)

    if not lint_res:
        # not passed
        logger.error("Linting was unsuccessfull.")
        return 1
    else:
        logger.info("Linting was successfull")

    # proto linter
    buf_linter = subprocess.Popen(
            ['buf', 'lint', str(interface.service_path / "api")],
            stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    buf_linter.wait()
    stdout, _ = buf_linter.communicate()

    if buf_linter.returncode != 0:
        logger.error(f"Protobuf linting was unsuccessfull")
        logger.info(f"Logs: \n {stdout.decode('utf-8')}")
        return 1
    else:
        logger.info("Passed Protobuf linter!")
