from git import Repo
from glob import glob
import logging
import os
import shelve
from constants import LAST_REVISION_SYNC_KEY
import compile_protobufs
import pathlib
import utils


# From https://github.com/python/cpython/blob/3.10/Lib/pathlib.py
def custom_is_relative_to(self, *other):
    """Return True if the path is relative to another path or False.
    """
    try:
        self.relative_to(*other)
        return True
    except ValueError:
        return False


if not hasattr(pathlib.PurePath, 'is_relative_to'):
    pathlib.PurePath.is_relative_to = custom_is_relative_to


class PblangsFileNotFound(Exception):
    pass


def get_proto_file_and_add_to_compile(filepath, compile_table, type_list):
    """
    Here we want to distinguish between
    diffrent api's we support (api, events_api)
    """
    filepath = pathlib.Path(filepath)
    stem = filepath.stem
    suffix = filepath.suffix
    if suffix != ".proto":
        return None

    # we are limiting ourselfs to only support changes under
    # the services folder
    if not filepath.is_relative_to(utils.root_path() / "services"):
        return None

    # check if it realy exists
    if not filepath.exists():
        return None

    # get the languages to compile
    pblangs_path = filepath.parent.parent.parent / "pblangs"
    api_type = filepath.parent.parent.parent.stem
    if not pblangs_path.exists():
        raise PblangsFileNotFound(
                f"""Couldn't find pblangs file for
                    service {stem} {pblangs_path}""")

    langs_content = pblangs_path.read_text()
    langs_list = langs_content.strip().split('\n')
    obj = {str(filepath): langs_list}
    if api_type in compile_table:
        compile_table[api_type][stem] = obj
    else:
        compile_table[api_type] = {stem: obj}

    logging.info(f"Found {stem}: {filepath}")
    type_list.append(str(filepath))


def sync():
    repo = utils.repo()

    logger = logging.getLogger(__package__)

    compile_proto_files_table = {}

    # get files that changed between these two revisions
    with shelve.open(str(utils.root_path() / ".shelve")) as ks:
        last_revision_sha = ks[LAST_REVISION_SYNC_KEY] \
            if LAST_REVISION_SYNC_KEY in ks else None

    curr_revision_sha = repo.head.object.hexsha

    diff_protobufs = []
    logging.info(f"""{'-'*20}
                     Checking diff from
                     {last_revision_sha} to
                     {curr_revision_sha}
                     {'-'*20}
                     """)

    if curr_revision_sha != last_revision_sha:
        # get the diff between them
        diff_files = repo.index.diff(last_revision_sha)
        for file in diff_files:
            if file.deleted_file:
                # TODO: Should we delete it from local api
                continue

            try:
                get_proto_file_and_add_to_compile(
                        utils.root_path() / file.a_path,
                        compile_proto_files_table, diff_protobufs)

            except PblangsFileNotFound as e:
                logger.error(e)
                return 1

    # get modified files
    modified_list = repo.head.commit.diff(None)
    modified_protofiles = []
    logging.info(f"{'-'*20} Checking modified files {'-'*20}")
    for file in modified_list:
        if file.deleted_file:
            # TODO: Should we delete it from local api
            continue

        try:
            get_proto_file_and_add_to_compile(
                    utils.root_path() / file.a_path,
                    compile_proto_files_table, modified_protofiles)

        except PblangsFileNotFound as e:
            logger.error(e)
            return 1

    # get unstaged files
    untracked_files = repo.untracked_files
    proto_untracked_files = []
    logging.info(f"{'-'*20} Checking untracked files {'-'*20}")
    for file in untracked_files:
        try:
            get_proto_file_and_add_to_compile(
                    utils.root_path() / file,
                    compile_proto_files_table, proto_untracked_files)

        except PblangsFileNotFound as e:
            logger.error(e)
            return 1

    if not compile_proto_files_table:
        logger.info("Nothing to sync.")
        return 0

    if 'api' in compile_proto_files_table:
        # Check for API breaking changes
        logger.info("Checking API breaks")
        api_broke = utils.api_broke(compile_proto_files_table['api'])
        breakpoint()

        logger.info("Compiling API...")
        try:
            compile_protobufs.compile_api(compile_proto_files_table['api'])
        except compile_protobufs.ModifyException as e:
            logger.error(e)
            return 1
        except compile_protobufs.CompileError as e:
            logger.error(e)
            return 1
        except Exception as e:
            logger.error(f"Compiling protobufs failed due to: {e}")
            return 1

    # add untracked proto files
    if len(proto_untracked_files) > 0:
        logger.info(f"""Adding ({', '.join(proto_untracked_files)})
                        to tracked files""")

        repo.index.add(proto_untracked_files)

    # add modified proto files
    if len(modified_protofiles) > 0:
        logger.info(f"Adding ({', '.join(modified_protofiles)})")
        repo.index.add(modified_protofiles)

    # logger.info("Commiting sync changes")
    # repo.index.commit(
        # "Synced ({', '.join(compile_proto_files_table.keys()})")

    # save commit sha
    with shelve.open(str(utils.root_path() / ".shelve")) as ks:
        last_revision_sha = repo.head.object.hexsha
        logger.info(f"Saving last revision sha {last_revision_sha} in cache")
        ks[LAST_REVISION_SYNC_KEY] = last_revision_sha

    return 0
