import pycodestyle


def linter(interface):
    py_files = [str(p) for p in list(interface.service_path.glob('**/*.py'))]

    style = pycodestyle.StyleGuide()
    result = style.check_files(py_files)

    return result.total_errors == 0
