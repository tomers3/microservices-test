"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: {{ create_date }}
"""

import unittest
import grpc

import grpc_testing
from api.{{ service_name_snake_case }}.v1 import {{ service_name_snake_case }}_pb2
from src.{{ service_name_snake_case }} import {{ service_name_pascal_case }}Servicer

class Test{{ service_name_pascal_case }}(unittest.TestCase):
    """
    Base class for testing {{ service_name_snake_case }} service
    """
    def setUp(self, _) -> None:
        super().__init__()
        servicers = {
            {{ service_name_snake_case }}_pb2.DESCRIPTOR.services_by_name['{{ service_name_pascal_case }}Service']: {{ service_name_pascal_case }}Servicer()
        }
        self.test_server = grpc_testing.server_from_dictionary(
            servicers, grpc_testing.strict_real_time())

    def test_send_greet(self):
        request = {{ service_name_snake_case }}_pb2.SendGreetRequest(
            msg='test',
        )
        method = self.test_server.invoke_unary_unary(
            method_descriptor=({{ service_name_snake_case }}_pb2.DESCRIPTOR
                .services_by_name['{{ service_name_pascal_case }}Service']
                .methods_by_name['SendGreet']),
            invocation_metadata={},
            request=request, timeout=1)

        response, metadata, code, details = method.termination()
        self.assertTrue(bool(response.status))
        self.assertEqual(code, grpc.StatusCode.OK)
