"""
This file was generated automaticlly by the Insoundz service CLI!

Created at: {{ create_date }}
"""
import insoundz_infra
import api.{{ service_name_snake_case }}.v1.{{ service_name_snake_case }}_pb2_grpc as {{ service_name_snake_case }}_grpc
# import api.{{ service_name_snake_case }}.v1.{{ service_name_snake_case }}_pb2 as {{ service_name_snake_case }}_pb


class {{ service_name_pascal_case }}Servicer({{ service_name_snake_case }}_grpc.{{ service_name_pascal_case }}Service, insoundz_infra.BaseService):
    """
    {{ service_name_pascal_case }}Servicer Doc string

    Implemet me
    """
    _service_servicer__ = staticmethod({{ service_name_snake_case }}_grpc.add_{{ service_name_pascal_case }}ServiceServicer_to_server)
