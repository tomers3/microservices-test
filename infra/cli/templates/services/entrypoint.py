"""
This file was generated automaticlly by the Insoundz service CLI!
No need to touch!

Created at: {{ create_date }}
"""
import logging
from src import {{ service_name_snake_case }}

SERVICE_VERSION = "0.1.0"

if __name__ == "__main__":
    import sys
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    logging.info("Version %s", SERVICE_VERSION)
    service = {{ service_name_snake_case }}.{{ service_name_pascal_case }}Servicer()
    service.start_service()
