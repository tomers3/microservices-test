#!/usr/bin/python3

import sys
from termcolor import colored
import argparse
import operations
import pyfiglet
import logging
import colorlog
import interfaces

usage_description = """
    How to create a service?
    ----------------------------
    dev create {service_name} {proto_file} (The proto file is optional).
    E.g: dev create_service example
    E.g: dev create_service example example.proto

    How to sync your service with the api package?
    -------------------------------------------------
    dev sync {service_name}
    E.g: dev sync example
"""

def create_handler():
    import inquirer

    interfaces_mapping = {
        'Microservice': interfaces.ServiceInterface
    }

    questions = [
      inquirer.List('service_type',
        message="What kind of service you want to create?",
        choices=list(interfaces_mapping.keys()),
      ),
      inquirer.Text('name', message="What's the name of your service"),
    ]

    answers = inquirer.prompt(questions)
    interface = interfaces_mapping[answers['service_type']](answers['name'], creating=True)
    
    return interface

operations_mapping = {
    "create": create_handler,
    "sync": operations.sync,
    "debug": operations.debug,
    "test": operations.test_service,
    "health": operations.health
}


if __name__ == "__main__":
    logger = logging.getLogger(__package__)
    logger.setLevel(logging.INFO)

    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(colorlog.ColoredFormatter(
        '%(log_color)s %(levelname)s %(message)s',
        log_colors={
                'DEBUG':    'green',
		'INFO':     'white',
		'WARNING':  'yellow',
		'ERROR':    'red',
		'CRITICAL': 'red,bg_white',
	},
	style='%'
    ))
    logger.addHandler(sh)

    __header = pyfiglet.figlet_format("Insoundz Services CLI", font="slant")
    print(colored(__header, 'red'))


    parser = argparse.ArgumentParser(description="Insoundz service CLI", usage=usage_description, formatter_class=argparse.RawTextHelpFormatter)
    required = parser.add_argument_group('Required Arguments')
    optional = parser.add_argument_group('Optional Arguments')

    required.add_argument('operation', metavar='operation', type=str,
                        help=(f"The operation to perform. available operations: ({','.join(operations_mapping.keys())})"))
    optional.add_argument('name', metavar='name', type=str, nargs='?',
                        help=(f"The service to operate on"))

    args = parser.parse_args()
    try:
        op = operations_mapping[args.operation]
    except KeyError as e:
        logger.error(f"[{args.operation}] is not a valid operation")
        logger.error(f"Available operations: ({','.join(operations_mapping.keys())})")
        exit(1)

    if args.operation == "create":
        interface = op()
        op = operations.create_service
    elif args.operation == "sync" or args.operation == "health":
        exit(op())
    else:
        # by default we will look for services under the microservice tag
        name = args.name
        if name is None:
            logger.error("Please provide the name of the service to operate on")
            exit(1)

        interface = interfaces.ServiceInterface(name)

    exit(op(interface))

