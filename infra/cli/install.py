#!/usr/bin/python3

import pyfiglet
import pathlib
from termcolor import colored
import stat
import os
import utils
import logging
import sys
import colorlog
from constants import LAST_REVISION_SYNC_KEY
import shelve

if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(colorlog.ColoredFormatter('%(log_color)s %(levelname)s %(message)s'))
    logger.addHandler(sh)

    __header = pyfiglet.figlet_format("Insoundz Services CLI", font="slant")
    print(colored(__header, 'red'))

    # set current revision sha in cache
    with shelve.open(str(utils.root_path() / ".shelve")) as ks:
        if LAST_REVISION_SYNC_KEY not in ks:
            last_revision_sha = utils.repo().head.object.hexsha
            logger.info(f"Saving last revision sha {last_revision_sha} in cache")
            ks[LAST_REVISION_SYNC_KEY] = last_revision_sha

    logger.info("Done.")
