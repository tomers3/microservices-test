import logging
import os
import fileinput
import sys


def modifier(service, pb_grpc_path, _, version):
    """
    Here we assume that the protobuf compiler has succeded
    so we will just modify the contents of these file to our needs
    """
    logger = logging.getLogger(__package__)

    replace_with = f"from ."
    match_with = f"from {service}.api.{service}.{version}"
    logger.info(f"Modifying intermediate file {pb_grpc_path}")
    for line in fileinput.input(pb_grpc_path, inplace=True):
        line = line.replace(match_with, replace_with)
        sys.stdout.write(line)

    # insert __init__ files
    try:
        (pb_grpc_path.parent / "__init__.py").touch()
        (pb_grpc_path.parent.parent / "__init__.py").touch()
    except Exception as e:
        raise e
