import pathlib
from distutils.dir_util import copy_tree
import tempfile
import utils
import os
import logging
import subprocess
from . import protobuf_modifiers


class LannguageIsNotSupported(Exception):
    pass


class ModifyException(Exception):
    pass


class CompileError(Exception):
    pass


class MoveFilesException(Exception):
    pass


def compile_api(compile_table):
    """
    Will compile proto files into using our docker image
    according to the compile_table
    """
    logger = logging.getLogger(__package__)

    # add all of the services into the include path
    process_pool = {}
    for service, protobuf_metadata in compile_table.items():
        protobuf_path = pathlib.Path(next(iter(protobuf_metadata)))
        langs = protobuf_metadata[str(protobuf_path)]

        logger.info(f"""Syncing {service} using protobuf file
                        {protobuf_path} compiling for ({','.join(langs)})""")

        langs_proc_pool = {}

        tmp_folder = tempfile.TemporaryDirectory()
        for lang in langs:
            if not hasattr(protobuf_modifiers, lang):
                raise LannguageIsNotSupported(
                        f"""The language {lang} is not supported!
                            Please check {service} pblangs file!""")

            cmd = [
                'python3', '-m', 'grpc_tools.protoc',
                '-I', str(protobuf_path.parent.parent.parent.parent.parent),
                '-I', str(protobuf_path.parent),
                f'--{lang}_out', tmp_folder.name,
                f'--grpc_{lang}_out', tmp_folder.name,
                str(protobuf_path)
            ]
            logger.info(f"Running {' '.join(cmd)}")

            proc = subprocess.Popen(
                    cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            langs_proc_pool[lang] = proc

        process_pool[service] = (langs_proc_pool, tmp_folder)

    for service, (lang_procs, tmp_folder) in process_pool.items():
        for lang, proc in lang_procs.items():
            exit_code = proc.wait()
            if exit_code != 0:
                # error
                _, stderr = proc.communicate()
                raise CompileError(f"""Couldn't compile {service} for
                                       language {lang}:
                                       {stderr.decode('utf-8')}""")
            else:
                logger.info(f"Successfully Compiled {service} {lang}.")

            modifier = getattr(protobuf_modifiers, lang).modifier
            base_path = pathlib.Path(tmp_folder.name)
            base_path = base_path / service / "api" / service

            # we assume there will only be one folder
            version = os.listdir(base_path)[0]

            tmp_pb_grpc_path = base_path / version / f"{service}_pb2_grpc.py"
            tmp_pb_path = base_path / version / f"{service}_pb2.py"
            service_api_path = utils.root_path() / "api" / lang / "api"

            try:
                modifier(
                        service,
                        tmp_pb_grpc_path,
                        tmp_pb_path,
                        version
                    )
            except Exception as e:
                raise ModifyException(f"""Couldn't modify {service},
                                          {lang} due to: {e}""")

            # move from tmp files into the api folder
            try:
                copy_tree(str(base_path.parent), str(service_api_path))
            except Exception as e:
                raise MoveFilesException(f"""Couldn't move the folder
                                            {str(base_path)} to
                                            {str(service_api_path)}""")

            logging.info(f"Finished Syncing {service} {lang}.")
