import utils


def test_camel_to_snake():
    assert utils.camel_to_snake("123") == "123"
    assert utils.camel_to_snake("123test") == "123test"
    assert utils.camel_to_snake("123_test") == "123_test"
    assert utils.camel_to_snake("123_Test") == "123_test"
    assert utils.camel_to_snake("123_TeSt") == "123_te_st"
    assert utils.camel_to_snake("test") == "test"
    assert utils.camel_to_snake("Test") == "test"


def test_snake_to_pascal():
    assert utils.snake_to_pascal("test") == "Test"
    assert utils.snake_to_pascal("123test") == "123Test"
    assert utils.snake_to_pascal("Test_Case") == "TestCase"
    assert utils.snake_to_pascal("test_case") == "TestCase"
    assert utils.snake_to_pascal("TestCase") == "Testcase"
    assert utils.snake_to_pascal("123_test") == "123Test"


def test_root_path():
    import pathlib
    import os
    match_with = pathlib.Path(
            os.path.dirname(os.path.realpath(__file__))
            ).parent.parent.parent
    assert utils.root_path() == match_with


def test_repo_path():
    import pathlib
    import os
    import git
    repo = utils.repo()
    assert type(repo) == git.Repo
    match_with = str(pathlib.Path(
        os.path.dirname(os.path.realpath(__file__))
        ).parent.parent.parent)
    assert repo.working_dir == match_with
