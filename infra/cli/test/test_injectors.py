import docker_packages_injectors
import utils
import pytest

python_docker_file_must_match = f"""FROM insoundz-python-base-image:latest
{docker_packages_injectors.python.inject}
"""


class TestInjectors:

    def test_python_injector(self):
        dockerfile_path = utils.root_path() \
                / "infra" / "cli" / "test" / "dockerfile_for_testing"

        res = docker_packages_injectors.python.injector(dockerfile_path)
        is_valid = res.startswith(python_docker_file_must_match)
        assert is_valid
