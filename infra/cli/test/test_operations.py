import pytest
import utils
import interfaces
import operations
import os
import shutil


@pytest.fixture()
def interface():
    interface = interfaces.ServiceInterface("testing_cli", creating=True)
    yield interface
    shutil.rmtree(utils.root_path() / "services" / "testing_cli")


@pytest.fixture()
def service():
    interface = interfaces.ServiceInterface("testing_cli", creating=True)
    operations.create_service(interface)
    yield interface
    shutil.rmtree(utils.root_path() / "services" / "testing_cli")


class TestOperations:

    def test_create_except_interface(self):
        with pytest.raises(AttributeError):
            operations.create_service("sadasd")

    def test_create_success(self, interface):
        assert operations.create_service(interface) == 0

    def test_create_exists(self, service):
        assert operations.create_service(service) == 5
