import pycodestyle
import os
import utils


class TestLinting():
    def test_linting(self):
        ignore_folders = ["templates"]
        cli_path = utils.root_path() / "infra" / "cli"
        dirs = os.listdir(cli_path)
        py_files = []
        for dir in dirs:
            if dir not in ignore_folders:
                dir_py_files = list((cli_path / dir).glob('**/*.py'))
                [py_files.append(str(p)) for p in dir_py_files]

        style = pycodestyle.StyleGuide()
        result = style.check_files(py_files)
        assert result.total_errors == 0
