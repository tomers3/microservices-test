import compile_protobufs
import shutil
import pytest
import utils

base_path = utils.root_path() / "infra" / "cli" / "test" / "protobufs"


def proto_path_template(name):
    return base_path / name / "api" / name / "v1" / f"{name}.proto"


@pytest.fixture()
def create_api():
    yield {"first": {str(proto_path_template("first")): ["python"]}}
    shutil.rmtree(utils.root_path() / "api" / "python" / "api" / "first")


class TestCompileProtobufs:

    def test_success(self, create_api):
        compile_protobufs.compile_api(create_api)

    def test_compiler(self):
        with pytest.raises(compile_protobufs.CompileError):
            compile_protobufs.compile_api(
                    {"second":
                        {str(proto_path_template("second")): ["python"]}})

    def test_lang(self):
        with pytest.raises(compile_protobufs.LannguageIsNotSupported):
            compile_protobufs.compile_api(
                    {"second":
                        {str(proto_path_template("second")):
                            ["not_existing_lang"]}})

    def test_servicename_dont_match(self):
        with pytest.raises(FileNotFoundError):
            compile_protobufs.compile_api(
                    {"first2":
                        {str(proto_path_template("first")): ["python"]}})
