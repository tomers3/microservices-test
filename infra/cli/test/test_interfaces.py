import pytest
import utils
import shutil
import operations
import interfaces


@pytest.fixture()
def interface():
    interface = interfaces.ServiceInterface(
            "testing_cli_interface", creating=True)
    operations.create_service(interface)
    yield interface
    shutil.rmtree(utils.root_path() / "services" / "testing_cli_interface")


class TestInterfaces:
    def test_exists(self, interface):
        with pytest.raises(interfaces.ServiceAlreadyExists):
            interfaces.ServiceInterface("testing_cli_interface", creating=True)

    def test_not_found(self):
        with pytest.raises(interfaces.ServiceNotFound):
            interfaces.ServiceInterface("not_existing_service")

    def test_not_found_metadata(self, interface):
        import os
        os.remove(interface.service_path / "metadata.json")
        with pytest.raises(interfaces.MetadataNotFound):
            interfaces.ServiceInterface("testing_cli_interface")
