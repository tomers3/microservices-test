import docker_packages_injectors

inject = f"""
RUN pip install grpcio
RUN pip install grpcio-tools

COPY infra/libs/entrypoints/python /tmp/__entrypoint
RUN cd /tmp/__entrypoint && python setup.py install

COPY api/python /tmp/__api
RUN cd /tmp/__api && python setup.py install
"""
# We need to see overtime where is the best place to inject those lines
# so we can maximize our cache hits
insert_at_the_end = False


def injector(dockerfile_path):
    lines = []
    with open(dockerfile_path, mode='r') as f:
        for line in f.readlines():
            lines.append(line)

    if insert_at_the_end:
        iterator = enumerate(reversed(lines))
        keyword = "ENTRYPOINT"
    else:
        # We are using this one
        iterator = enumerate(lines)
        keyword = "FROM"

    _, line = next(iterator)
    while True:
        if line.startswith(keyword):
            next_idx, _ = next(iterator)
            lines.insert(next_idx, inject)
            return ''.join(lines)
        try:
            _, line = next(iterator)
        except StopIteration:
            raise docker_packages_injectors.KeywordNotFound(keyword)
