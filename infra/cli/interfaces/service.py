import utils
import json
import pydantic


class ServiceAlreadyExists(Exception):
    pass


class ServiceNotFound(Exception):
    pass


class MetadataNotFound(Exception):
    pass


class Metadata(pydantic.BaseModel):
    language: str


class ServiceInterface():

    def __init__(self, name, creating=False):
        self.name = name

        # Service path
        self.service_path = utils.root_path() / "services" / name
        if self.service_path.exists():
            if creating:
                raise ServiceAlreadyExists()
        elif not creating:
            raise ServiceNotFound()

        if not creating:
            # get the service metadata
            try:
                with open(self.service_path / "metadata.json") as f:
                    # validate metadata
                    self.metadata = Metadata(**json.load(f))
            except OSError:
                raise MetadataNotFound()

        # create pascal case and snake case from the given 'service_name'
        self.snake_case = utils.camel_to_snake(name)
        self.pascal_case = utils.snake_to_pascal(self.snake_case)
