import asyncio
import grpc

class BaseService():
    def __init__(self):
        pass

    async def _serve(self):
        assert hasattr(self, '_service_servicer__'), '_service_servicer__ is not defined!'
        server = grpc.aio.server()
        self._service_servicer__(self, server)

        server.add_insecure_port('[::]:50051')
        await server.start()
        await server.wait_for_termination()

    def start_service(self):
        asyncio.get_event_loop().run_until_complete(self._serve())

