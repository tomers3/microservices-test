from setuptools import setup

setup(
      name='insoundzBaseService',
      version='1.0',
      description='Insoundz Base service interface',
      py_modules=['insoundz_infra'],
      author='Insoundz'
 )
