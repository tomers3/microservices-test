import asyncio
import grpc

class BaseService():
    __service_servicer = None

    def __init__(self):
        pass

    async def _serve(self):
        breakpoint()
        assert self.__service_servicer is None, "__service_servicer cannot be None!"
        server = grpc.aio.server()
        self.__service_servicer(
                self, server
        )

        server.add_insecure_port('[::]:50051')
        await server.start()
        await server.wait_for_termination()

    def start_service(self):
        asyncio.get_event_loop().run_until_complete(self._serve())

